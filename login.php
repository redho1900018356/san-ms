<?php 
include 'config.php';
include 'function.php';

if (isset($_POST['login'])) {
	$login = login($_POST['user'], $_POST['pw'], $con);
}
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>SAN Management System - Login Panel</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/loginStyle.css">
</head>
<body>
<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->
    <h2 class="active"> Login</h2>
    <h2 class="inactive underlineHover"><a style="color: black; font-weight: bold;" href="daftar.php">Daftar</a> </h2>

    <!-- Icon -->
    <div class="fadeIn first">
      <img src="https://www.upload.ee/image/5940782/SAN.png" id="icon" alt="User Icon" />
    </div>

    <!-- Login Form -->
    <form action="" method="post">
      <input type="text" id="login" class="fadeIn second" name="user" placeholder="Username">
      <input type="password" id="password" class="fadeIn third" name="pw" placeholder="Password">
      <input type="submit" class="fadeIn fourth" value="Log In" name="login">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover">Lupa Password? Hubungi redok!</a>
    </div>

  </div>

  <?php if (isset($login)): ?>
  	<div class="alert alert-danger" style="margin-top: 2em" role="alert">
 		<?php echo $login ?>
	</div>
    <?php endif ?>
</div>
</body>
</html>